public class CompteLettre {


    public static int comptelettreTriage(String phrase, char[] triage) {
        /**
         * Cette méthode compte le nombre de caractères présents dans une phrase qui correspondent
         * aux caractères spécifiés dans le tableau de triage.
         *
         * @param phrase La phrase dans laquelle effectuer le comptage.
         * @param triage Le tableau de caractères à rechercher dans la phrase.
         * @return Le nombre total de caractères correspondant au tableau de triage dans la phrase.
         */
        int totalvoyelle = 0;

        for (int x = 0; x < phrase.length(); x++) {
            char caractere = phrase.charAt(x);

            // Vérifie si le caractère est présent dans le tableau de triage
            if (VerifLettre.estLettre(caractere, triage)) {
                totalvoyelle++;
            }
        }
        return totalvoyelle;
    }
}