

public class VerifLettre {
    /**
     * Cette classe contient une méthode pour vérifier si un caractère est présent dans un tableau de triage.
     */

     public static boolean estLettre(char caractere, char[] triage) {
        /**
         * Vérifie si le caractère est présent dans le tableau de triage.
         *
         * @param caractere Le caractère à vérifier.
         * @param triage    Le tableau de triage contenant les caractères à comparer.
         * @return true si le caractère est présent dans le tableau de triage, sinon false.
         */
        // Vérifie si le caractère est présent dans le tableau de triage
        for (char c : triage) {
            if (caractere == c) {
                return true;
            }
        }
        return false;
    }
}