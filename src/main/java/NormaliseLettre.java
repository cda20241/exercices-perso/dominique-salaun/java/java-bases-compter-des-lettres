import java.text.Normalizer;

public class NormaliseLettre {


    public static String norma(String phrase) {
        /**
         * Cette méthode normalise une chaîne de caractères en effectuant les étapes suivantes :
         * 1. Convertir la chaîne en minuscules.
         * 2. Supprimer les espaces en début et en fin de chaîne.
         * 3. Supprimer les accents de la chaîne.
         *
         * @param phrase La chaîne de caractères à normaliser.
         * @return La chaîne de caractères normalisée.
         */
        // Convertir en minuscules
        phrase = phrase.toLowerCase();
        // Supprimer les espaces en début et en fin de chaîne
        phrase = phrase.trim();
        // Supprimer les accents
        phrase = Normalizer.normalize(phrase, Normalizer.Form.NFD);
        // Retourner la phrase normalisée
        return phrase;
    }
}