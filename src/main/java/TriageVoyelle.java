import java.util.Scanner;

public class TriageVoyelle {


    public static void triage() {
        /**
         * Cette méthode effectue le triage des voyelles dans une phrase donnée par l'utilisateur.
         * Elle demande à l'utilisateur d'entrer une phrase, normalise les caractères de la phrase,
         * puis compte le nombre de chaque voyelle ('a', 'e', 'i', 'o', 'u', 'y') dans la phrase.
         * Enfin, elle affiche le nombre de chaque voyelle et le nombre total de voyelles dans la phrase.
         */

        Scanner scanner = new Scanner(System.in);

        System.out.print("Entrez une phrase : ");
        String phrase = scanner.nextLine();

        // Normalise les caractères de la phrase pour le traitement
        phrase = NormaliseLettre.norma(phrase);

        // Tableau des voyelles à trier
        char[] triage = {'a', 'e', 'i', 'o', 'u', 'y'};

        int totalVoyelles = 0;

        // Parcours des voyelles
        for (char lettre : triage) {
            int nombreDeLettres = CompteLettre.comptelettreTriage(phrase, new char[]{lettre});
            System.out.println("Nombre de '" + lettre + "' : " + nombreDeLettres);
            totalVoyelles += nombreDeLettres;
        }

        System.out.println("Nombre total de voyelles : " + totalVoyelles);

        scanner.close();
    }
}
